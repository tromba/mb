# -*- coding: utf-8 -*-
"""
Created on Sun Jul 20 20:17:42 1969

@author: Thilo
"""
# -----------
# - imports -
# -----------
import numpy as np
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from PIL import Image

# --------------------
# - input parameters -
# --------------------
D_cuts = np.pi / 3
N_cuts = 3
x_max = 2 * np.pi
rad_0 = 0.3
rad_1 = 0.5
rad_2 = 1.5
rad_3 = 1.6
rad_4 = 2.0
points = 1001
dpi = 1000
font = {"family": "serif", "color": "red", "weight": "bold", "size": 23}

# ------------------------------------------
# - set up arrays in cartesian coordinates -
# ------------------------------------------
x = np.linspace(0, x_max, points)
r_0 = np.full(points, rad_0)
r_1 = np.full(points, rad_1)
r_2 = np.full(points, rad_2)
r_3 = np.full(points, rad_3)
r_4 = np.full(points, rad_4)

# ---------------
# - adjust data -
# ---------------
# cut out sections
r_2[x % (x_max / N_cuts) > D_cuts] = rad_1
# rotate such that first black section is symmetric around x
x -= x_max * (sum(r_2 == max(r_2)) / N_cuts) / points / 2
# rotate everything by +90°F
x += np.pi / 2

# -----------------
# - make the plot -
# -----------------
fig = Figure(figsize=(4, 4), dpi=dpi)
canvas = FigureCanvas(fig)
ax = fig.add_subplot(111, projection="polar")
ax.fill_between(x, 0, r_3, alpha=1, color="yellow", lw=0)
ax.fill_between(x, 0, r_0, alpha=1, color="black", lw=0)
ax.fill_between(x, r_1, r_2, alpha=1, color="black", lw=0)
ax.fill_between(x, r_3, r_4, alpha=1, color="black", lw=0)
ax.axis("off")
ax.text(np.pi * 1 / 2, (rad_1 + rad_2) / 2.5, "GO", fontdict=font, ha="center")
ax.text(np.pi * 3 / 2, (rad_1 + rad_2) / 2.5, "NUCLEAR", fontdict=font, ha="center")
canvas.draw()

# ---------------------------------------------
# - extract figure with non-white pixels only -
# ---------------------------------------------
# get size information on figure
width, height = fig.get_size_inches() * fig.get_dpi()
# cast the data to an RGB array
raw_img = np.frombuffer(canvas.tostring_rgb(), dtype=np.uint8).reshape(
    int(height), int(width), 3
)
# break the read-only status
raw_img = raw_img.copy()
# replace some of the black pixels with yellow to emulate wear - sucks
"""
test = 0
for i in range(int(points**2 / 100)):
    # generate random pixel position
    x = int(np.random.random() * width)
    y = int(np.random.random() * height)
    # check whether this pixel is black
    if np.array_equal(raw_img[y,x,:], np.array([0,0,0])):
        do you seriously read this?
        for j in range(-1,1):
            for k in range(-1,1):
                raw_img[y - j,x - k,:] = np.array([255,255,0]) #np.full(3, (np.random.random() * 0.5 + 0.5) * 255)
"""
# draw updated figure
img = Image.fromarray(raw_img.astype("uint8"), "RGB")
if True:
    img.show()
    
# ----------------
# - save locally -
# ----------------
try:
    path = "C:/Users/Thilo/Desktop/git/gud/"
    img.save(path + "go_nuclear.pdf")
    img.save(path + "go_nuclear.png")
except FileNotFoundError:
    pass

# ------------------
# - make animation -
# ------------------
# needs some fancy modules which are annoying to install
try:
    from matplotlib.animation import MakeAnimation
    # define some inputs for the animation
    inputs = np.geomspace(1, 1337, 42)
    sense  = 12
    non    = np.zeros((1337, 1337, 1337, 1337))
    # make the animation based on those
    anim = MakeAnimation(img, non, sense, inputs)
    
# if those aren't available, simply show the animation on youtube
except ImportError:
    import webbrowser
    # not as fancy as storing them to your personal google drive
    url = "https://www.youtube.com/watch?v=iik25wqIuFo"
    webbrowser.open(url)
